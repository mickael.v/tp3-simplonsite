#TP3 - Site Simplon Grenoble

##TP numéro 3 formation dév web et web mobile.

###5 pages réalisées:
-index.html - L'accueil avec navbar - carousel - temoignages - footer
-NosFormations.html - Formations : dev web - dev data - refuggek
-NotreMethode.html - Agile
-contact.html - page contact - newsletter - map
-Plan.html - plan du site

-Maquette - https://www.figma.com/file/hncl2KBiHsYkfCTFhN2LbR/Maquette-2?node-id=0%3A1


###Groupe :

Kevin
Mickael


###Objectif :

Créer un mini site pour le centre de formation Simplon de la ville de la promo. Le site devra présenter la formation Simplon ainsi que donner les informations utiles sur la fabrique de Simplon.
Vous devez respecter la charte graphique du groupe Simplon (couleur, police d'écriture, etc ...). Vous devez aussi intégrer le logo du Simplon.
Aucune maquette graphique n'a été définit pour le site, vous avez carte blanche.
Le site devra être réalisé en HTML et CSS.

Le site doit être responsive
Vous devez utiliser SASS pour la création de votre CSS
Vous devez utiliser une convention de nommage
Le site doit contenir un formulaire de contact, non fonctionnel mais les champs sont validés :

Nom
Prenom

Genre (Mr / Mme / Autre)
Email
Message (max 300 caractères)




Les images doivent être optimisées pour le web (taille adaptée / poids etc)
Les champs dédiés au référencement doivent être complétés (images et meta)
Vous pouvez utiliser un framework CSS : Bootstrap ou autre
Vous pouvez utiliser toutes les librairies CSS ou JS que vous voulez


Retour
Dépôt présent sur GitLab.
À rendre pour le 03 juillet 2020 au soir.
